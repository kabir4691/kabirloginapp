package com.kabir.login;

import android.app.Application;

public class KabirLoginApplication extends Application {

    private static KabirLoginApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static synchronized KabirLoginApplication getInstance() {
        return instance;
    }
}
