package com.kabir.login.utils.sharedpreferences;

interface Constants {

    String KEY_IS_USER_LOGGED_IN = "is_user_logged_in";
    String KEY_USER_ID = "user_id";
    String KEY_PASSWORD = "password";
}
