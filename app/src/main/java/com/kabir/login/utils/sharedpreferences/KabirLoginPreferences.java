package com.kabir.login.utils.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.kabir.login.KabirLoginApplication;

public class KabirLoginPreferences implements Constants {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private String FILE_NAME = "kabir_login_preferences";

    public KabirLoginPreferences() {
        sharedPreferences = KabirLoginApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    @NonNull
    private SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    @NonNull
    private SharedPreferences.Editor getEditor() {
        if (editor == null) {
            editor = getSharedPreferences().edit();
        }
        return editor;
    }

    public boolean getIsUserLoggedIn() {
        return getSharedPreferences().getBoolean(KEY_IS_USER_LOGGED_IN, false);
    }

    public KabirLoginPreferences setIsUserLoggedIn(boolean value) {
        getEditor().putBoolean(KEY_IS_USER_LOGGED_IN, value)
                   .commit();
        return this;
    }

    public String getUserId() {
        return getSharedPreferences().getString(KEY_USER_ID, null);
    }

    public KabirLoginPreferences setUserId(String value) {
        getEditor().putString(KEY_USER_ID, value)
                   .commit();
        return this;
    }

    public String getPassword() {
        return getSharedPreferences().getString(KEY_PASSWORD, null);
    }

    public KabirLoginPreferences setPassword(String value) {
        getEditor().putString(KEY_PASSWORD, value)
                   .commit();
        return this;
    }
}
