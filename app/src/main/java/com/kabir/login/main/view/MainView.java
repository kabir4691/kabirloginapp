package com.kabir.login.main.view;

public interface MainView {

    void showLoginView(boolean isShown);
    void showLogoutView(boolean isShown);

    void showInvalidUserIdError();
    void showInvalidPasswordError();

    void showLoginFailedError();
}
