package com.kabir.login.main.presenter;

public interface MainPresenter {

    void start();

    void onLoginRequested(String userId, String password);
    void onLogoutRequested();
}
