package com.kabir.login.main.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.kabir.login.main.view.MainView;
import com.kabir.login.utils.sharedpreferences.KabirLoginPreferences;

public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;

    private KabirLoginPreferences preferences;

    public MainPresenterImpl(@NonNull MainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void start() {
        boolean isUserLoggedIn = getPreferences().getIsUserLoggedIn();
        if (isUserLoggedIn) {
            mainView.showLoginView(false);
            mainView.showLogoutView(true);
            return;
        }

        mainView.showLoginView(true);
        mainView.showLogoutView(false);
    }

    @Override
    public void onLoginRequested(String userId, String password) {
        if (TextUtils.isEmpty(userId)) {
            mainView.showInvalidUserIdError();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            mainView.showInvalidPasswordError();
            return;
        }

        if (!userId.equals("1")) {
            mainView.showLoginFailedError();
            return;
        }

        if (!password.equals("2")) {
            mainView.showLoginFailedError();
            return;
        }

        getPreferences().setIsUserLoggedIn(true)
                        .setUserId(userId)
                        .setPassword(password);

        mainView.showLoginView(false);
        mainView.showLogoutView(true);
    }

    @Override
    public void onLogoutRequested() {
        getPreferences().setIsUserLoggedIn(false);

        mainView.showLoginView(true);
        mainView.showLogoutView(false);
    }

    @NonNull
    private KabirLoginPreferences getPreferences() {
        if (preferences == null) {
            preferences = new KabirLoginPreferences();
        }
        return preferences;
    }
}
