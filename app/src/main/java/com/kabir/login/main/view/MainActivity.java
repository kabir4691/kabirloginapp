package com.kabir.login.main.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kabir.login.R;
import com.kabir.login.main.presenter.MainPresenter;
import com.kabir.login.main.presenter.MainPresenterImpl;

public class MainActivity extends AppCompatActivity implements MainView {

    private LinearLayout loginLayout;
    private EditText userIdEditText;
    private EditText passwordEditText;
    private TextView submitTextView;
    private TextView logoutTextView;

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        loginLayout = findViewById(R.id.login_layout);
        userIdEditText = findViewById(R.id.user_id_edit_text);
        passwordEditText = findViewById(R.id.password_edit_text);
        submitTextView = findViewById(R.id.submit_text_view);
        logoutTextView = findViewById(R.id.logout_text_view);

        submitTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onLoginRequested(userIdEditText.getText().toString(),
                                           passwordEditText.getText().toString());
            }
        });

        logoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onLogoutRequested();
            }
        });

        presenter = new MainPresenterImpl(this);
        presenter.start();
    }

    @Override
    public void showLoginView(boolean isShown) {
        loginLayout.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showLogoutView(boolean isShown) {
        logoutTextView.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showInvalidUserIdError() {
        userIdEditText.setError("Invalid user ID");
    }

    @Override
    public void showInvalidPasswordError() {
        passwordEditText.setError("Invalid password");
    }

    @Override
    public void showLoginFailedError() {
        Toast.makeText(this, "Invalid login credentials", Toast.LENGTH_SHORT).show();
    }
}
